# NextLabs Assignment
##### Deploy Link : https://nextlabs-assignment.netlify.app/



## Building an Interactive Website with vanilla JS


## Description
Welcome to our interactive web application powered by vanilla JavaScript! Our website is designed to simplify user management, offer efficient user deletion, and provide a seamless product catalog browsing experience with the advantage of lazy loading.

### Tech Stack : 
- Vanila.js
- Bootstrap
- HTML
- CSS





### Features :

```
Add New Users
Delete Users
User List Display
Lazy Loading

```

### Landing Page


 ![Alt Text](https://gitlab.com/Ga1156/nextlabs/-/raw/main/Images/Screenshot__1925_.png) 

#### Section A

 ![Alt Text](https://gitlab.com/Ga1156/nextlabs/-/raw/main/Images/Screenshot__1922_.png) 


#### Section B

 ![Alt Text](https://gitlab.com/Ga1156/nextlabs/-/raw/main/Images/Screenshot__1923_.png) 

#### Mobile View

![Alt Text](https://gitlab.com/Ga1156/nextlabs/-/raw/main/Images/Screenshot__1924_.png) 


#### Duration: 11/09/2023 - 12/09/2023


@nextlabs.com
