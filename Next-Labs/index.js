console.log('index.js')
const button = document.getElementById('myButton');


const modal = document.getElementById('myModal');
const openModalBtn = document.getElementById('openModalBtn');
const closeModalBtn = document.getElementById('closeModalBtn');
const form = document.getElementById('myForm');

openModalBtn.addEventListener('click', () => {
    // console.log('open')
    modal.style.display = 'block';
});

closeModalBtn.addEventListener('click', () => {
    modal.style.display = 'none';
});

window.addEventListener('click', (event) => {
    if (event.target === modal) {
        modal.style.display = 'none';
    }
});

form.addEventListener('submit', (event) => {
    event.preventDefault();
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const comment = document.getElementById('comment').value;







    let obj = {
        name,
        email,
        comment,
        currentTime: new Date().toISOString()
    }

    if (!name.trim() || !comment.trim()) {
        return swal("Please fill correct credentials!", "")

    }


    console.log(obj)
    fetch(`https://lumpy-husky-address.glitch.me/products`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj)
    }).then((res) => {
        if (res.ok) {
            getdata()
        }
    })
    getdata()
    modal.style.display = 'none';


    swal("User Added Successfully", " ", "success")

});

let data = []

const getdata = async () => {
    let res = await fetch(`https://lumpy-husky-address.glitch.me/products`)
    let data = await res.json()
    getUserData(data)
}

getdata()
function getUserData(data) {
    const UserBody = document.getElementById('user-body')
    // console.log(data);
    const Userdata = data.map((user) =>
        `<div class="col-md-4">     
            <div class="card p-3 mb-2">
                <div class="d-flex justify-content-between">
                    <div class="d-flex flex-row align-items-center">
                        <div class="icon"><i class="bx bxl-mailchimp"></i> </div>
                        
                        
                        <div class="ms-2 c-details">
                            <h6 class="mb-0">${user.name}</h6> <span>${user.currentTime.split('T')[0]}</span>
                        </div>
                    </div>
                    
                    <div class="badge" onclick={handledlt(${user.id})}> <span> <i class='bx bx-x red-icon custom-icon-size'></i>User</span> </div>
                    
                    
                </div>
                
                <div class="mt-3">
                    <p class="heading">Comments :<br>${user.comment}</p>

                </div>
                <div>
                    <p class="heading">Email :<br>${user.email}</p>

                </div>
            </div>
        </div>`



    ).join('');


    UserBody.innerHTML = Userdata;

}

function handledlt(id) {
    console.log('id', id)

    fetch(`https://lumpy-husky-address.glitch.me/products/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',

        },
    })
        .then(response => {
            if (response.ok) {

                getdata()
                swal("User Deleted", "", "error")

            } else {

                console.error('Error deleting resource');
            }
        })
        .catch(error => {
            console.error('Network error:', error);
        });

}



const productList = document.querySelector('.product-container');
const loadingMessage = document.getElementById('loading-message');

let currentPage = 1;
const perPage = 9;
let isLoading = false;

function fetchAndDisplayProducts() {
    // console.log('current-page', currentPage)

    isLoading = true;
    loadingMessage.style.display = 'block';
    fetch(`https://dummyjson.com/products?limit=${perPage}&page=${currentPage}`)
        .then((response) => response.json())
        .then((data) => {
            // console.log('ff',data.products)
            data.products.forEach((product) => {
                const productCard = document.createElement('div');
                productCard.classList.add('product-card');
                productCard.innerHTML = `
                     <img id="product-img" src=${product.thumbnail} >   
                    <h4 class="product-title">${product.title}</h4>
                    <p >${product.description.slice(0, 50) + "..."}</p>
                    <p class="product-price">Price: $${product.price}</p>
                    <button class="buy-button">Buy Now</button>
                `;
                productList.append(productCard);
            });
            isLoading = false;
            loadingMessage.style.display = 'none';

        })
        .catch((error) => {
            console.error('Error fetching data:', error);
            isLoading = false;
        });
}

function loadMoreProducts() {
    if (isLoading) return; // Do not load more if already loading

    const scrollPosition = window.innerHeight + window.scrollY;
    const pageHeight = document.body.scrollHeight;

    if (scrollPosition >= pageHeight - 1000) {
        currentPage++;
        fetchAndDisplayProducts();
    }
}

window.addEventListener('scroll', loadMoreProducts);

fetchAndDisplayProducts();